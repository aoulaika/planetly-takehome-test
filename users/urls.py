from django.urls import path

from .views import current_user, UserCreate, UserDetailDelete

urlpatterns = [
    path('current', current_user, name='current-user'),
    path('', UserCreate.as_view(), name="users-list"),
    path('<int:pk>', UserDetailDelete.as_view(), name="user-detail"),
]

from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'password', 'username', 'first_name', 'last_name', 'email',)

    def create(self, validated_data):
        u = self.Meta.model(**validated_data)
        u.password = make_password(validated_data['password'])
        u.save()
        return u

    def update(self, u, validated_data):
        if 'password' in validated_data:
            u.password = make_password(validated_data['password'])
        u.save()
        return u

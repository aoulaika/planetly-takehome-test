from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase


class UsersTest(APITestCase):
    url_list = reverse('users-list')
    url_detail = reverse('user-detail', args=[1])
    current_user = reverse('current-user')

    @classmethod
    def setUpTestData(cls):
        # Set up data for the whole TestCase
        cls.owner = User.objects.create(id=1, username='admin', password='root')
        cls.not_owner = User.objects.create(id=2, username='admin2', password='root2')
        cls.admin = User.objects.create(id=3, username='admin3', password='root3', is_superuser=True)

    # Tests POST /users
    def test_create_user_with_no_username(self):
        data = {
            'username': '',
            'email': 'foobarbaz@example.com',
            'password': 'foobar'
        }

        response = self.client.post(UsersTest.url_list, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_user_with_invalid_email(self):
        data = {
            'username': '',
            'email': 'foobarbazexamplecom',
            'password': 'foobar'
        }

        response = self.client.post(UsersTest.url_list, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_user_ok(self):
        data = {
            'username': 'testuser',
            'email': 'user@example.com',
            'password': 'testuser'
        }

        self.assertEqual(User.objects.count(), 3)
        response = self.client.post(UsersTest.url_list, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 4)

    # Tests PATCH /users/1
    def test_edit_user_unauthenticated(self):
        data = {
            'username': 'testuser',
            'email': 'user@example.com',
            'password': 'testuser'
        }

        response = self.client.patch(UsersTest.url_detail, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_edit_other_user_unauthorized(self):
        self.client.force_login(self.not_owner)
        data = {
            'username': 'testuser',
            'email': 'user@example.com',
            'password': 'testuser'
        }

        response = self.client.patch(UsersTest.url_detail, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_edit_own_user_authorized(self):
        self.client.force_login(self.owner)
        data = {
            'username': 'test',
            'password': 'secret_password',
        }

        response = self.client.patch(UsersTest.url_detail, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    # Tests DELETE /users
    def test_delete_user_unauthenticated(self):
        data = {
            'username': 'testuser',
            'email': 'user@example.com',
            'password': 'testuser'
        }

        response = self.client.delete(UsersTest.url_detail, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete_user_not_allowed(self):
        self.client.force_login(self.not_owner)
        data = {
            'username': 'testuser',
            'email': 'user@example.com',
            'password': 'testuser'
        }

        response = self.client.delete(UsersTest.url_detail, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_user_only_deactivate(self):
        self.client.force_login(self.owner)
        self.assertEqual(User.objects.count(), 3)
        response = self.client.delete(UsersTest.url_detail, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(User.objects.get(pk=1).is_active, False)
        self.assertEqual(User.objects.count(), 3)

    # Tests GET /users/current
    def test_get_current_user_authenticated(self):
        self.client.force_login(self.owner)
        response = self.client.get(UsersTest.current_user, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_current_user_unauthenticated(self):
        response = self.client.get(UsersTest.current_user, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    # Tests GET /users
    def test_get_users_as_admin(self):
        self.client.force_login(self.admin)
        response = self.client.get(UsersTest.url_list, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_users_without_admin(self):
        self.client.force_login(self.owner)
        response = self.client.get(UsersTest.url_list, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

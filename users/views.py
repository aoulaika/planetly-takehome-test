from django.contrib.auth.models import User
from rest_framework import generics, permissions, decorators, response
from rest_framework.permissions import SAFE_METHODS
from rest_framework.response import Response

from .serializers import UserSerializer


@decorators.api_view(['GET'])
def current_user(request):
    """
    Determine the current user by their token, and return their data
    """

    serializer = UserSerializer(request.user)
    return response.Response(serializer.data)


class IsOwner(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to get and edit it.
    """

    def has_object_permission(self, request, view, obj):
        return obj == request.user


class CreateOrListAdminOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method == "POST":
            return True

        elif request.user.is_superuser and request.method in SAFE_METHODS:
            return True

        return False


class UserCreate(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (CreateOrListAdminOnly,)


class UserDetailDelete(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwner)

    def delete(self, request, *args, **kwargs):
        u = self.get_object()
        u.is_active = False
        u.save()
        return Response(status=204)

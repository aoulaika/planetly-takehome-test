FROM python:3.10
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
# Add a work directory
WORKDIR /app
# Cache and Install dependencies
COPY Pipfile .
COPY Pipfile.lock .
RUN pip install pipenv && pipenv install --system
# Copy app files
COPY . .
# Expose port
EXPOSE 3000
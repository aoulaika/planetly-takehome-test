from datetime import datetime

from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from usage_types.models import UsageType
from usages.models import Usage


class UsagesTest(APITestCase):
    url_list = reverse('usages-list')
    url_detail = reverse('usage-detail', args=[1])

    @classmethod
    def setUpTestData(cls):
        # Set up data for the whole TestCase
        cls.owner = User.objects.create(id=1, username='admin', password='root')
        cls.not_owner = User.objects.create(id=2, username='admin2', password='root2')
        cls.admin = User.objects.create(id=3, username='admin3', password='root3', is_superuser=True)
        UsageType.objects.bulk_create([
            UsageType(id=900, name="electricity", unit="kwh", factor=1.5),
            UsageType(id=901, name="water", unit="kg", factor=26.93),
            UsageType(id=902, name="heating", unit="kwh", factor=3.892),
            UsageType(id=903, name="heating", unit="l", factor=8.57),
            UsageType(id=904, name="heating", unit="m3", factor=19.456),
        ])
        Usage.objects.bulk_create([
            Usage(usage_type_id=900, amount=10, user_id=1),
            Usage(usage_type_id=901, amount=15, user_id=1),
            Usage(usage_type_id=902, amount=20, user_id=1),
            Usage(usage_type_id=903, amount=25, user_id=1),
            Usage(usage_type_id=904, amount=30, user_id=1),
            Usage(usage_type_id=904, amount=30, user_id=2),
        ])

    # Tests GET /usages
    def test_get_usages_unauthenticated(self):
        response = self.client.get(UsagesTest.url_list, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_admin_get_usages(self):
        self.client.force_login(self.admin)
        response = self.client.get(UsagesTest.url_list, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(dict(response.data)['count'], 6)

    def test_admin_get_usages_with_range_filter(self):
        self.client.force_login(self.admin)
        response = self.client. \
            get(UsagesTest.url_list, {'date_lt': datetime.now(), 'date_gt': datetime.now()}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_usages_authenticated(self):
        self.client.force_login(self.owner)
        response = self.client.get(UsagesTest.url_list, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(dict(response.data)['count'], 5)

    def test_get_usages_authenticated_not_owner(self):
        self.client.force_login(self.not_owner)
        response = self.client.get(UsagesTest.url_list, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(dict(response.data)['count'], 1)

    def test_get_usages_authenticated_admin(self):
        self.client.force_login(self.admin)
        response = self.client.get(UsagesTest.url_list, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(dict(response.data)['count'], 6)

    # Tests POST /usages
    def test_post_usages_unauthenticated(self):
        response = self.client.post(UsagesTest.url_list, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_post_usages_invalid(self):
        self.client.force_login(self.owner)
        response = self.client.post(UsagesTest.url_list, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_post_usages_invalid_amount(self):
        self.client.force_login(self.owner)
        response = self.client.post(UsagesTest.url_list, {'amount': 'invalid'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_post_usages_valid(self):
        self.client.force_login(self.owner)
        data = {
            'amount': 10,
            'user': 1,
            'usage_type': 900,
            'usage_at': datetime.now()
        }
        response = self.client.post(UsagesTest.url_list, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    # Tests PATCH /usages
    def test_edit_usages_unauthenticated(self):
        response = self.client.patch(UsagesTest.url_detail, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_edit_usages_unauthorized(self):
        self.client.force_login(self.not_owner)
        response = self.client.patch(UsagesTest.url_detail, {'amount': 10}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_edit_usages_ok(self):
        self.client.force_login(self.owner)
        response = self.client.patch(UsagesTest.url_detail, {'amount': 10}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

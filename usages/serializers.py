from rest_framework import serializers

from .models import Usage


class UsageSerializer(serializers.ModelSerializer):
    usage_type_name = serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='usage_type.name'
    )

    class Meta:
        model = Usage
        fields = '__all__'

    def create(self, validated_data):
        u = Usage(**validated_data)
        u.emissions = u.amount * u.usage_type.factor
        u.save()
        return u

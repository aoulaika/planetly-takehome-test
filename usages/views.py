from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, permissions
from rest_framework import pagination

from .models import Usage
from .serializers import UsageSerializer


class IsOwner(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to get and edit it.
    """

    def has_object_permission(self, request, view, obj):
        return obj.user == request.user


class CustomPagination(pagination.PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 50


class UsageList(generics.ListCreateAPIView):
    queryset = Usage.objects.all()
    serializer_class = UsageSerializer
    permission_classes = (permissions.IsAuthenticated,)
    pagination_class = CustomPagination
    filter_backends = (DjangoFilterBackend,)

    def post(self, request, *args, **kwargs):
        self.request.data['user'] = self.request.user.id
        return self.create(request, *args, **kwargs)

    def filter_queryset(self, queryset):
        f = {}
        if not self.request.user.is_superuser:
            f.update({'user': self.request.user})

        if usage_at__gte := self.request.query_params.get('usage_at__gte'):
            f.update({'usage_at__gte': usage_at__gte})

        if usage_at__lte := self.request.query_params.get('usage_at__lte'):
            f.update({'usage_at__lte': usage_at__lte})

        return Usage.objects.all().filter(**f)


class UsageDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Usage.objects.all()
    serializer_class = UsageSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwner)

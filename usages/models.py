from django.contrib.auth.models import User
from django.db import models
from django.utils.timezone import now

from usage_types.models import UsageType


# Create your models here.
class Usage(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    usage_type = models.ForeignKey(UsageType, on_delete=models.DO_NOTHING)
    usage_at = models.DateTimeField(blank=True, default=now)
    amount = models.FloatField(blank=False)
    emissions = models.FloatField(blank=True, default=0, editable=False)

    class Meta:
        db_table = 'usages'
        ordering = ('-usage_at',)

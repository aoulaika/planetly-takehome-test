from django.urls import path

from .views import UsageList, UsageDetail

urlpatterns = [
    path('', UsageList.as_view(), name='usages-list'),
    path('<int:pk>', UsageDetail.as_view(), name='usage-detail'),
]

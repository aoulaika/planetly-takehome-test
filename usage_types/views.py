from rest_framework import generics, permissions

from .models import UsageType
from .serializers import UsageTypeSerializer


class CreateAdminOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method == "POST" and not request.user.is_superuser:
            return False

        return True


class AdminOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        if not request.user.is_superuser:
            return False

        return True


class UsageTypeList(generics.ListCreateAPIView):
    queryset = UsageType.objects.all()
    serializer_class = UsageTypeSerializer
    permission_classes = (permissions.IsAuthenticated, CreateAdminOnly)


class UsageTypeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = UsageType.objects.all()
    serializer_class = UsageTypeSerializer
    permission_classes = (permissions.IsAuthenticated, AdminOnly,)

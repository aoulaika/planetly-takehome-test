from rest_framework import serializers

from .models import UsageType


class UsageTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = UsageType
        fields = '__all__'

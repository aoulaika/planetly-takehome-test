from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from usage_types.models import UsageType


class UsageTypesTests(APITestCase):
    url_list = reverse('usage-types-list')
    url_detail = reverse('usage-type-detail', args=[900])

    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create(id=1, username='admin', password='root')
        cls.admin = User.objects.create(id=2, username='admin2', password='root2', is_superuser=True)
        UsageType.objects.bulk_create([
            UsageType(id=900, name="electricity", unit="kwh", factor=1.5),
            UsageType(id=901, name="water", unit="kg", factor=26.93),
            UsageType(id=902, name="heating", unit="kwh", factor=3.892),
            UsageType(id=903, name="heating", unit="l", factor=8.57),
            UsageType(id=904, name="heating", unit="m3", factor=19.456),
        ])

    # normal user can only retrieve usage types
    def test_list_usage_types_unauthenticated(self):
        response = self.client.get(UsageTypesTests.url_list, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_list_usage_types_authenticated(self):
        self.client.force_login(self.user)
        response = self.client.get(UsageTypesTests.url_list, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_usage_type_unauthenticated(self):
        response = self.client.post(UsageTypesTests.url_list, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_usage_type_unauthenticated_unauthorized(self):
        self.client.force_login(self.user)
        response = self.client.post(UsageTypesTests.url_list, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_usage_type_unauthenticated(self):
        response = self.client.delete(UsageTypesTests.url_detail, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete_usage_type_authenticated_unauthorized(self):
        self.client.force_login(self.user)
        response = self.client.delete(UsageTypesTests.url_detail, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_patch_usage_type_unauthenticated(self):
        response = self.client.patch(UsageTypesTests.url_detail, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_patch_usage_type_authenticated_unauthorized(self):
        self.client.force_login(self.user)
        response = self.client.patch(UsageTypesTests.url_detail, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    # admin can delete, edit and create
    def test_admin_create_usage_type_invalid(self):
        self.client.force_login(self.admin)
        data = {}
        response = self.client.post(UsageTypesTests.url_list, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_admin_create_usage_type(self):
        self.client.force_login(self.admin)
        data = {
            'name': 'test', 'unit': 'l', 'factor': 1.2
        }
        response = self.client.post(UsageTypesTests.url_list, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_admin_patch_usage_type(self):
        self.client.force_login(self.admin)
        response = self.client.patch(UsageTypesTests.url_detail, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_admin_delete_usage_type(self):
        self.client.force_login(self.admin)
        response = self.client.delete(UsageTypesTests.url_detail, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

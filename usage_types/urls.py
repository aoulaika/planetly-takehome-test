from django.urls import path

from .views import UsageTypeList, UsageTypeDetail

urlpatterns = [
    path('', UsageTypeList.as_view(), name='usage-types-list'),
    path('<int:pk>', UsageTypeDetail.as_view(), name='usage-type-detail'),
]

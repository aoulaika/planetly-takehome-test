from django.db import models


# Create your models here.
class UsageType(models.Model):
    name = models.CharField(max_length=50, blank=False)
    unit = models.CharField(max_length=10, blank=False)
    factor = models.FloatField(blank=False)

    class Meta:
        db_table = 'usage_types'

    def __str__(self):
        return self.name

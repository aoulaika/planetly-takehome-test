from django.apps import AppConfig


class UsageTypesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'usage_types'

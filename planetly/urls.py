"""planetly URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import permissions
from rest_framework.schemas import get_schema_view
from rest_framework_simplejwt.views import TokenObtainPairView

urlpatterns = [
    path('', get_schema_view(
        title="Your Project",
        description="API for all things …",
        version="1.0.0",
        public=True,
        permission_classes=(permissions.AllowAny,),
    )),
    path('admin/', admin.site.urls),
    path('api/auth', TokenObtainPairView.as_view()),
    path('api/users/', include('users.urls')),
    path('api/usages/', include('usages.urls')),
    path('api/usage_types/', include('usage_types.urls')),
]

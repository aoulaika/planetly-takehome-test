## How to run the app

The project can be run using docker-compose provided on how to run back-end and front-end

## How to run unit test

````shell
python manage.py test
````

## How much time it took me

I don't know exactly how much time took me since I worked on it few minutes or hours on a daily basis, but I would
estimate a full day of work

## Challenges

1. remembering how to use django after not working with python for around 3 years

## What can be improved or added

1. test most invalid request cases
2. run tests on docker
3. retrieve usage_type.name when getting usages
4. separate dev requirements from app ones
5. use postgres instead of sqlite
6. double check .gitignore & .dockerignore as I've done them quickly
